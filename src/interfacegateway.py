#Khrapov A. S. M17-512
#Encoding UTF-8

from datetime import time, date

from patient import *
from specialization import Specialization
from doctor import Doctor
from timeelement import TimeElement

class InterfaceGateway:
  def __init__(self, doctor, patient, specialization, visit):
    self.doctor = doctor
    self.patient = patient
    self.specialization = specialization
    self.visit= visit

  ###GET
  def getSpecializationName(self):
    return self.specialization.getName()

  def getSpecializationServiceTime(self):
    return self.specialization.getServiceTime()

  def getPatientName(self):
    return self.patient.getFullName()

  def getPatientOms(self):
    return self.patient.getOms()

  def getDoctorName(self):
    return self.doctor.getFullName()

  def getDoctorSpecializations(self):
    return list(map(lambda sc : sc.getName(), self.doctor.getSpecializations()))

  def getTimeTable(self):
    return self.doctor.getTimeElements()

  def getEnrollTime(self):
    return (self.visit.getDate(), self.visit.getStartTime())

  def getUnenrollTime(self):
    return (self.visit.getDate(), self.visit.getStartTime())

  ###SEND
  def sendError(self, message):
    if not (type(message) is str):
      raise TypeError("Invalid call sendError: operand type of message is not str")  
    raise TypeError(message)

  def sendSpecializationsName(self, specsName):
    if not (type(specsName) is list):
      raise TypeError("Invalid call sendSpecializationsName: operand type of specsName is not list")  
    for i in specsName:
      if not (type(i) is str):
        raise TypeError("Invalid call sendSpecializationsName: operand type of elements of specsName is not str")
    return True

  def sendDoctorsName(self, doctorsList):
    if not (type(doctorsList) is list):
      raise TypeError("Invalid call sendDoctors: operand type of doctorsList is not list")
    for i in doctorsList:
      if not (type(i) is str):
        raise TypeError("Invalid call sendDoctors: operand type of elements of doctorsList is not str")
    return True

  def sendDoctorTimeTable(self, timeElements):
    if not (type(timeElements) is list):
      raise TypeError("Invalid call sendDoctorTimeTable: operand type of timeElements is not TimeElement")
    for i in timeElements:
      if not (type(i) is tuple):
        raise TypeError("Invalid call sendDoctorTimeTable: operand type of elements of timeElements is not tuple")
      if not (type(i[0]) is int):
        raise TypeError("Invalid call sendFreeTimes: operand type of column 0 of element of timeElements is not int")
      if not (type(i[1]) is time):
        raise TypeError("Invalid call sendFreeTimes: operand type of column 1 of element of timeElements is not time")
      if not (type(i[2]) is int):
        raise TypeError("Invalid call sendFreeTimes: operand type of column 2 of element of timeElements is not int")
      if not (type(i[3]) is str):
        raise TypeError("Invalid call sendFreeTimes: operand type of column 2 of element of timeElements is not str")

    return True

  def sendFreeTimes(self, freeTimes):
    if not (type(freeTimes) is list):
      raise TypeError("Invalid call sendFreeTimes: operand type of freeTimes is not list")
    for i in freeTimes:
      if not (type(i) is tuple):
        raise TypeError("Invalid call sendFreeTimes: operand type of element of freeTimes is not tuple")
      if not (type(i[0]) is date):
        raise TypeError("Invalid call sendFreeTimes: operand type of column 0 of element of freeTimes is not date")
      if not (type(i[1]) is time):
        raise TypeError("Invalid call sendFreeTimes: operand type of column 1 of element of freeTimes is not time")
      if not (type(i[2]) is int):
        raise TypeError("Invalid call sendFreeTimes: operand type of column 2 of element of freeTimes is not int")

    return True

  def sendEngagedTime(self, engagedTimes):
    if not (type(engagedTimes) is list):
      raise TypeError("Invalid call sendEngagedTime: operand type of engagedTime is not list")
    for i in engagedTimes:
      if not (type(i) is tuple):
        raise TypeError("Invalid call sendEngagedTime: operand type of element of freeTimes is not tuple")
      if not (type(i[0]) is date):
        raise TypeError("Invalid call sendEngagedTime: operand type of column 0 of element of freeTimes is not date")
      if not (type(i[1]) is time):
        raise TypeError("Invalid call sendEngagedTime: operand type of column 1 of element of freeTimes is not time")
      if not (type(i[2]) is int):
        raise TypeError("Invalid call sendEngagedTime: operand type of column 2 of element of freeTimes is not int")

    return True


