#Khrapov A. S. M17-512
#Encoding UTF-8

from datetime import timedelta
from sqlalchemy import Column, Integer, String, TIMESTAMP, BigInteger,ForeignKey
from sqlalchemy.orm import relationship

from database import Base

class Specialization(Base):

  #_name = "Urolog"
  #_serviceTime = timedelta(minutes=1)


  __tablename__ = 'specialization'
  id = Column(Integer, primary_key=True)
  _name = Column(String(50), unique=False)
  _time = Column(BigInteger, unique=False)

  doctor_id = Column(Integer, ForeignKey('doctor.id'))




  def __repr__(self):
    return {
      "name" : self.getName(),
      "time" : self.getServiceTime()
    }


  def __init__(self, nameValue, serviceTimeMinutes):
    if not (type(nameValue) is str):
      raise TypeError("Invalid init class Specialization: operand type of nameValue is not str")

    if not (type(serviceTimeMinutes) is int):
      serviceTimeMinutes = 0
   

    self._name = nameValue
    self._serviceTime = timedelta(minutes=serviceTimeMinutes)
    self._time = serviceTimeMinutes

  def setName(self, value):
    if not (type(value) is str):
      raise TypeError("Invalid call setName in class Specialization: operand type of nameValue is not str")

    self._name = value

  def setServiceTime(self, valueMinutes):

    if not (type(valueMinutes) is int):
      raise TypeError("Invalid call setServiceTime in class Specialization: operand type of valueMinutes is not int")

    if (valueMinutes <= 0):
      raise ValueError("Invalid call setServiceTime in class Specialization: value of valueMinutes less or equal zero")

    if (valueMinutes >= 1440):
      raise ValueError("Invalid call setServiceTime in class Specialization: value of valueMinutes more or equal twenty-four hours")

    self._serviceTime = timedelta(minutes=valueMinutes)

  def getName(self):
    return self._name

  def getServiceTime(self):
    return self._time
