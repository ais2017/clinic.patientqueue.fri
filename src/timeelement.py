#Khrapov A. S. M17-512
#Encoding UTF-8

from datetime import time, timedelta, datetime

from specialization import Specialization

from sqlalchemy import Column, Integer, String, TIME, ForeignKey
from database import Base
from sqlalchemy.orm import relationship

class TimeElement(Base):


  #_weekDay = 0
  #_startTime = time(8, 30, 0)
  #_visitsCount = 10
  #_specialization = Specialization("Urolog", 10)

  __tablename__ = 'timeelement'
  id = Column(Integer, primary_key=True)
  _weekDay = Column(Integer)
  _startTime = Column(TIME)
  _visitsCount = Column(Integer)

  specialization_id = Column(Integer, ForeignKey('specialization.id'), nullable=False)
  doctor_id = Column(Integer, ForeignKey('doctor.id'))


  _specialization = relationship("Specialization")

  def __init__(self, weekDayValue, startTimeValue, visitsCountValue, specializationValue):

    if not (type(weekDayValue) is int):
      raise TypeError("Invalid init class TimeElement: operand type of weekDayValue is not int")

    if not (type(startTimeValue) is time):
      raise TypeError("Invalid init class TimeElement: operand type of startTimeValue is not time")

    if not (type(visitsCountValue) is int):
      raise TypeError("Invalid init class TimeElement: operand type of visitsCountValue is not int")

    if not (type(specializationValue) is Specialization) and None:
      raise TypeError("Invalid init class TimeElement: operand type of specializationValue is not Specialization")

    if (weekDayValue < 0) or (weekDayValue > 6):
      raise ValueError("Invalid init class TimeElement: value of weekDayValue is not in the range 0..6")

    if (visitsCountValue <= 0):
      raise ValueError("Invalid init class TimeElement: value of visitsCountValue less or equal zero")

    self._weekDay = weekDayValue
    self._startTime = startTimeValue
    self._visitsCount = visitsCountValue
    self._specialization = specializationValue

  def setWeekDay(self, value):
    if not (type(value) is int):
      raise TypeError("Invalid call setWeekDay in class TimeElement: operand type of value is not int")

    if (value < 0) or (value > 6):
      raise ValueError("Invalid call setWeekDay in class TimeElement: value of value is not in the range 0..6")

    self._weekDay = value

  def setStartTime(self, value):
    if not (type(value) is time):
      raise TypeError("Invalid call setStartTime in class TimeElement: operand type of value is not time")

    self._startTime = value

  def setVisitsCount(self, value):
    if not (type(value) is int):
      raise TypeError("Invalid call setVisitsCount in class TimeElement: operand type of value is not int")

    if (value <= 0):
      raise ValueError("Invalid call setVisitsCount in class TimeElement: value of value less or equal zero")

    self._visitsCount = value

  def setSpecialization(self, value):
    if not (type(value) is Specialization):
      raise TypeError("Invalid call setSpecialization in class TimeElement: operand type of value is not Specialization")

    self._specialization = value

  def getWeekDay(self):
    return self._weekDay

  def getStartTime(self):
    return self._startTime

  def getVisitsCount(self):
    return self._visitsCount

  def getEndTime(self):
    delta = timedelta(minutes = self._specialization.getServiceTime()*self._visitsCount)
    startDateTime = datetime(1980, 1, 1, hour=self._startTime.hour, minute=self._startTime.minute)
    endDataTime = startDateTime + delta
    endTime = time(endDataTime.hour, endDataTime.minute)
    return endTime 

  def getSpecialization(self):
    return self._specialization;

