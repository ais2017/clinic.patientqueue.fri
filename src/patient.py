#Khrapov A. S. M17-512
#Encoding UTF-8

from sqlalchemy import Column, Integer, String, TIMESTAMP
from database import Base


class Patient(Base):
  #_fullName = "Al Bom Quara"
  #_oms = "123-456"


  __tablename__ = 'patient'
  id = Column(Integer, primary_key=True)
  _fullName = Column(String(50), unique=False)
  _oms = Column(String(120), unique=False)

  def __init__(self, nameValue, omsValue):

    if not (type(nameValue) is str):
      raise TypeError("Invalid init class Patient: operand type of nameValue is not str")

    if not (type(omsValue) is str) and None:
      raise TypeError("Invalid init class Patient: operand type of omsValue is not str")

    self._fullName = nameValue
    self._oms = omsValue

  def setFullName(self, value):
    if not (type(value) is str):
      raise TypeError("Invalid call setFullName in class Patient: operand type of value is not str")
    self._fullName = value

  def setOms(self, value):
    if not (type(value) is str):
      raise TypeError("Invalid call setOms in class Patient: operand type of value is not str")
    self._oms = value

  def getFullName(self):
    return self._fullName

  def getOms(self):
    return self._oms
