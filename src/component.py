#Khrapov A. S. M17-512
#Encoding UTF-8

from datetime import datetime, date, timedelta

from dbgateway import DBGateway
from interfacegateway import InterfaceGateway

from timeelement import TimeElement
from patient import Patient
from specialization import Specialization
from doctor import Doctor

def createSpecialization(db, iface):
  if not isinstance(db, DBGateway):
    raise TypeError
  if not isinstance(iface, InterfaceGateway):
    raise TypeError

  specName = iface.getSpecializationName()
  specServiceTime = iface.getSpecializationServiceTime()

  if db.existSpecialization(specName):
    #send gui Error
    iface.sendError("Already exist specialization with this name.")
    return True

  newSpec = Specialization(specName, specServiceTime)
  db.saveSpecialization(newSpec)
  return True


def registerPatient(db, iface):
  if not isinstance(db, DBGateway):
    raise TypeError
  if not isinstance(iface, InterfaceGateway):
    raise TypeError

  patientName = iface.getPatientName()
  patientOms = iface.getPatientOms()

  if db.existPatient(patientName):
    #send gui Error
    iface.sendError("Already exist patient with this name.")
    return True

  newPatient = Patient(patientName, patientOms)
  db.savePatient(newPatient)
  return True


def registerDoctor(db, iface):
  if not isinstance(db, DBGateway):
    raise TypeError
  if not isinstance(iface, InterfaceGateway):
    raise TypeError

  #give all specializzations for combobox
  allSpecs = db.loadAllSpecializationsName()
  iface.sendSpecializationsName(allSpecs)

  doctorName = iface.getDoctorName()
  specNames = iface.getDoctorSpecializations()

  if db.existDoctor(doctorName):
    #send gui Error
    iface.sendError("Already exist doctor with this name.")
    return True

  specs = []
  for i in specNames:
    if db.existSpecialization(i):
      specs.append(db.loadSpecialization(i))
    else:
      #send gui Error
      iface.sendError("Don't exist specializations with this name.")
      return True

  newDoctor = Doctor(doctorName, specs)    
  db.saveDoctor(newDoctor)
  return True


def changeDoctorSpecializations(db, iface):
  if not isinstance(db, DBGateway):
    raise TypeError
  if not isinstance(iface, InterfaceGateway):
    raise TypeError

  allSpecs = db.loadAllSpecializationsName()
  iface.sendSpecializationsName(allSpecs)

  doctorName = iface.getDoctorName()
  specNames = iface.getDoctorSpecializations()

  #check
  if (db.existDoctor(doctorName)):
    doctor = db.loadDoctor(doctorName)
  else:
    #send gui Error
    iface.sendError("Don't exist doctor with this name.")
    return True
  

  specs = []
  for i in specNames:
    if db.existSpecialization(i):
      specs.append(db.loadSpecialization(i))
    else:
      #send gui Error
      iface.sendError("Don't exist specializations with this name.")
      return True
  
  doctor.setSpecializations(specs)
  db.saveDoctor(doctor)
  return True


def changeTimeTable(db, iface):
  if not isinstance(db, DBGateway):
    raise TypeError
  if not isinstance(iface, InterfaceGateway):
    raise TypeError

  doctorName = iface.getDoctorName()
  if (db.existDoctor(doctorName)):
    doctor = db.loadDoctor(doctorName)
  else:
    #send gui Error
    iface.sendError("Don't exist doctor with this name.")
    return True

  listTimeElements = doctor.getTimeElements()
  lisTimeElementCortages = []
  for i in listTimeElements:
    lisTimeElementCortages.append((i.getWeekDay(), i.getStartTime(), i.getVisitsCount(), i.getSpecialization().getName()))

  iface.sendDoctorTimeTable(lisTimeElementCortages)
  iface.sendSpecializationsName(doctor.getSpecializations())
  timeTablesCortagesList = iface.getTimeTable()
  timeTableList = []
  for i in timeTablesCortagesList:
    tSpec = db.loadSpecialization(i[3])
    timeTableList.append(TimeElement(i[0], i[1], i[2], tSpec))

  doctor.setTimeElements(timeTableList)
  db.saveDoctor(doctor)
  return True

def addTimeTable(db, time):
  if not isinstance(db, DBGateway):
    raise TypeError
  if not isinstance(time, TimeElement):
    raise TypeError

  db.saveTime(time)


def enroll(db, iface):
  if not isinstance(db, DBGateway):
    raise TypeError
  if not isinstance(iface, InterfaceGateway):
    raise TypeError

  patientName = iface.getPatientName()
  if (db.existPatient(patientName)):
    patient = db.loadPatient(patientName)
  else:
    #send gui Error
    iface.sendError("Don't exist patient with this name.")
    return True

  #send all specialization for user combobox
  allSpecs = db.loadAllSpecializationsName()
  iface.sendSpecializationsName(allSpecs)

  specName = iface.getSpecializationName()
  if db.existSpecialization(specName):
    spec = db.loadSpecialization(specName)
  else:
    #send gui Error
    iface.sendError("Don't exist specialization with this name.")
    return False

  doctorsList = db.loadDoctorsNameBySpecialization(specName)
  #chekc list size and type string

  #send list of doctors for combobox
  iface.sendDoctorsName(doctorsList)

  doctorName = iface.getDoctorName()
  if (db.existDoctor(doctorName)):
    doctor = db.loadDoctor(doctorName)
  else:
    #send gui Error
    iface.sendError("Don't exist doctor with this name.")
    return True



  freeTimes = getPermittedTime(doctor, patient, spec)

  iface.sendFreeTimes(freeTimes)
  enrollTime = iface.getEnrollTime()

  if db.existVisitPatient(patientName, enrollTime[0], enrollTime[1]):
    #send gui Error
    iface.sendError("This patient already enrolled in this date and time.")
    return True

  #cortage date time
  doctor.enrollPatient(patient, enrollTime[0], enrollTime[1])
  db.saveDoctor(doctor)


def getPermittedTime(doctor, patient, spec):
  days = []
  curdate = datetime.now()
  for i in range(7):
    curdate += timedelta(days=1)
    days.append(date(curdate.year, curdate.month, curdate.day))
  freeTimes = []
  for i in days:
    freeTimes.extend(doctor.getPermittedTime(i, spec, patient))
  return freeTimes


def unenroll(db, iface):
  if not isinstance(db, DBGateway):
    raise TypeError
  if not isinstance(iface, InterfaceGateway):
    raise TypeError

  patientName = iface.getPatientName()
  if (db.existPatient(patientName)):
    patient = db.loadPatient(patientName)
  else:
    #send gui Error
    iface.sendError("Don't exist patient with this name.")
    return True

  #send all specialization for user combobox
  allSpecs = db.loadAllSpecializationsName()
  iface.sendSpecializationsName(allSpecs)

  specName = iface.getSpecializationName()
  if db.existSpecialization(specName):
    spec = db.loadSpecialization(specName)
  else:
    #send gui Error
    iface.sendError("Don't exist specialization with this name.")
    return False

  doctorsList = db.loadDoctorsNameBySpecialization(specName)
  
  #send list of doctors for combobox
  iface.sendDoctorsName(doctorsList)

  doctorName = iface.getDoctorName()
  if (db.existDoctor(doctorName)):
    doctor = db.loadDoctor(doctorName)
  else:
    #send gui Error
    iface.sendError("Don't exist doctor with this name.")
    return True

  days = []
  curdate = datetime.now()
  for i in range(7):
    curdate += timedelta(days=1)
    days.append(date(curdate.year, curdate.month, curdate.day))

  engagedVisits = []
  for i in days:
    engagedVisits.extend( doctor.getEngagedTime(i, spec, patient) )

  iface.sendEngagedTime(engagedVisits)
  unenrollTime = iface.getUnenrollTime()

  #cortage date time
  doctor.unenrollPatient(patient, unenrollTime[0], unenrollTime[1])
  db.saveDoctor(doctor)


def loadDoctor(db, name):
  return db.loadDoctor(name)

def loadAllVisitByPatient(db, name):
  return db.loadVisitByPatientName(name)


#if __name__ == '__main__':
  #db = DBGateway()
  #iface = InterfaceGateway()
  #createSpecialization(db, iface)
  #registerPatient(db, iface)
  #registerDoctor(db, iface)
  #changeDoctorSpecializations(db, iface)
  #changeTimeTable(db, iface)
  #enroll(db, iface)
  #unenroll(db, iface)
