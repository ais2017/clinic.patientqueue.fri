#Khrapov A. S. M17-512
#Encoding UTF-8

from datetime import date
from datetime import time

from patient import Patient
from specialization import Specialization

from sqlalchemy import Column, Integer, String, TIME, ForeignKey, DATE
from database import Base
from sqlalchemy.orm import relationship

class Visit(Base):
  #_date = date(1980, 1, 1)
  #_startTime = time(8, 30, 0)
  #_specialization = Specialization("Urolog", 10)
  #_patient = Patient("Al Bom Quara", "123-456")


  __tablename__ = 'visit'
  id = Column(Integer, primary_key=True)
  _startTime = Column(TIME, unique=False)
  _date = Column(DATE, unique=False)

  specialization_id = Column(Integer, ForeignKey("specialization.id"))
  patinent_id = Column(Integer, ForeignKey("patient.id"))
  doctor_id = Column(Integer, ForeignKey('doctor.id'))

  _specialization = relationship("Specialization")
  _patient = relationship("Patient")

  def __init__(self, startTimeValue, dateValue, specializationValue, patientValue):

    if not (type(startTimeValue) is time) and None:
      raise TypeError("Invalid init class Visit: operand type of startTimeValue is not time")

    if not (type(dateValue) is date) and None:
      raise TypeError("Invalid init class Visit: operand type of dateValue is not date")

    if not (type(specializationValue) is Specialization) and None:
      raise TypeError("Invalid init class Visit: operand type of specializationValue is not Specialization")

    if not (type(patientValue) is Patient) and None:
      raise TypeError("Invalid init class Visit: operand type of patientValue is not Patient")
    
    self._startTime = startTimeValue
    self._date = dateValue
    #_weekDay = weekDayValue
    self._specialization = specializationValue
    self._patient = patientValue

  def setDate(self, value):
    if not (type(value) is date):
      raise TypeError("Invalid call setDate in class Visit: operand type of value is not date")

    self._date = value

  #def setWeekDay(self, value):
  #  _weekDay = value

  def setStartTime(self, value):
    if not (type(value) is time):
      raise TypeError("Invalid call setStartTime in class Visit: operand type of value is not time")

    self._startTime = value

  def setSpecialization(self, value):
    if not (type(value) is Specialization):
      raise TypeError("Invalid call setSpecialization in class Visit: operand type of value is not Specialization")

    self._specialization = value

  def setPatient(self, value):
    if not (type(value) is Patient):
      raise TypeError("Invalid call setPatient in class Visit: operand type of value is not Patient")
    self._patient = value

  def getDate(self):
    return self._date

  def getWeekDay(self):
    return self._date.weekday()

  def getStartTime(self):
    return self._startTime

  def getSpecialization(self):
    return self._specialization

  def getPatient(self):
    return self._patient
