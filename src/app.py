from flask import Flask

from flask import Flask, request, send_from_directory, render_template, jsonify
from flask import abort, redirect, url_for
from database import *
from flask import request, make_response
from component import *
from patient import Patient
from visit import Visit
from interfacegateway import *
from dbgateway import DBGateway
import json
from collections import namedtuple
from bson import json_util
import json
from json import dumps
from flask import Flask, session
from flask.ext.session import Session



app = Flask(__name__, template_folder="../templates")
SESSION_TYPE = 'filesystem'
app.config.from_object(__name__)
Session(app)


db = DBGateway()



@app.route("/patient", methods=["POST"])
def patient_service():
    name = request.json["name"]
    oms = request.json["oms"]
    patient = Patient(name, oms)
    gateway = InterfaceGateway(None, patient, None, None)
    registerPatient(db, gateway)
    return make_response("Ok")


@app.route("/patient/login", methods=["POST"])
def login():
    a = request
    name = request.json["name"]
    session['patientName'] = name
    return make_response("Ok")


@app.route("/patient/login", methods=["GET"])
def login_get():
    val = session.get("patientName")
    if(val is None):
        return jsonify("Empty")
    else:
        return jsonify(val)




@app.route("/doctor/register", methods=["POST"])
def doctor_service():
    name = request.json["doctorName"]
    json_object = request.json["specializations"];
    specializations = list(map(lambda json: Specialization(json["name"], json["serviceTime"]), json_object))
    doctor = Doctor(name, specializations)
    registerDoctor(db, InterfaceGateway(doctor, None, None, None))
    return make_response("Ok")


@app.route("/doctor/time/add", methods=["POST"])
def doctor_service_add():
    obj = request.json
    doctor = db.loadDoctor(obj["doctorName"])
    timeElement = TimeElement(obj["weekOfDay"], time(obj["hour"], obj["minute"]), obj['visitCount'],
                              db.loadSpecialization(obj["specialization_name"]))
    timeElement.doctor_id = doctor.id
    addTimeTable(db, timeElement)
    return make_response("Ok")


@app.route("/doctor/load", methods=["GET"])
def doctor_service_load():
    doctor = loadDoctor(db, request.args.get("doctorName"))
    test = {
        "name": doctor.getFullName(),
        "specialization": doctor.getSpecializations(),
        "timeElements": doctor.getTimeElements(),
        "visits": doctor.getVisits(),

    }
    return jsonify(test)


@app.route("/specialization/all", methods=["GET"])
def specialization_load():
    name = db.loadAllSpecializationsName()
    return jsonify(name)


@app.route("/specialization/select", methods=["POST"])
def specialization_select():
    a = request
    spec = request.json["type"]
    patientName = session.get("patientName")
    doctorName = request.json["doctorName"]
    dateTimeCheck = datetime.fromtimestamp(request.json["dateTime"])

    try:
        visit = enroll(db,
                   InterfaceGateway(Doctor(doctorName, None), Patient(patientName, None), Specialization(spec, None),
                                    Visit(dateTimeCheck.time(), dateTimeCheck.date(), None, None)))
    except TypeError as e:
        return e.args
    return jsonify(True)


@app.route("/specialization/save", methods=["POST"])
def specialization_save():
    specName = request.json["name"]
    specTime = request.json["time"]
    createSpecialization(db, InterfaceGateway(None, None, Specialization(specName, specTime), None))
    return jsonify(True)


@app.route("/doctor/free", methods=["GET"])
def doctors_get_free_space():
    specName = request.args.get("specName")
    patientName = session.get("patientName")
    doctors = list(map(lambda x: loadDoctor(db, x), db.loadDoctorsNameBySpecialization(specName)))
    permitted = []
    for doc in doctors:
        permitted.extend(getPermittedTime(doc, db.loadPatient(patientName), db.loadSpecialization(specName)))

    def myconverter(o):
        if isinstance(o, date):
            s = (datetime(o.year, o.month, o.day) - datetime(1970, 1, 1))
            return s.total_seconds()
        if isinstance(o, time):
            t= timedelta(minutes=o.hour * 60 + o.minute)
            return t.total_seconds()

    result = list(map(lambda x: (x[3],
                   ((datetime(x[0].year, x[0].month, x[0].day) - datetime(1970, 1, 1))
                    + timedelta(minutes=x[1].hour * 60 + x[1].minute)).total_seconds()), permitted))

    return json.dumps(result, default=myconverter)


@app.route("/doctor/uneroll", methods=["POST"])
def doctors_get_uneroll():
    workDate = datetime.fromtimestamp(request.json.get("dateTime"))
    specName = request.json.get("type")
    patientName = session.get("patientName")
    doctorName = request.json.get("doctorName")
    unenroll(db, InterfaceGateway(Doctor(doctorName, None), Patient(patientName, None), Specialization(specName, None),
                                  Visit(workDate.time(), workDate.date(), None, None)))
    return jsonify(True)


@app.route("/patient/registrations", methods=["GET"])
def load_all_visits_by_patient():
    patientName = session.get("patientName")
    visits = loadAllVisitByPatient(db, patientName)
    dto = []
    for visit in visits:
        if visit.doctor_id is None:
            continue
        curDate = (datetime(visit._date.year, visit._date.month, visit._date.day) - datetime(1970, 1, 1)
                    + timedelta(minutes=visit._startTime.hour * 60 + visit._startTime.minute)).total_seconds()
        doctor = db.loadDoctorById(visit.doctor_id)
        dto.append({
            "currentDate" : curDate,
            "specialization" : visit._specialization._name,
            "doctorName" : doctor._fullName
        })
    return jsonify(dto)


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('../static/js', path)


@app.route('/index')
def index_page():
    return render_template("index.html")


@app.route('/patient')
def patient_page():
    return render_template("patient.html")



@app.route('/login')
def login_page():
    return render_template("login.html")




@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('../static/css', path)


if __name__ == '__main__':
    app.run()
    init_db()
