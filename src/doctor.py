#Khrapov A. S. M17-512
#Encoding UTF-8

from datetime import timedelta, date, datetime, time

from patient import Patient
from specialization import Specialization
from visit import Visit
from timeelement import TimeElement

from sqlalchemy import Column, Integer, String, TIME, ForeignKey
from database import Base
from sqlalchemy.orm import relationship


class Doctor(Base):
  #_fullName = "Al Bom Quara"
  #_visits = []
  #_timeTable = []
  #_specializations = []

  __tablename__ = 'doctor'
  id = Column(Integer, primary_key=True)
  _fullName = Column(String(50))



  _specializations = relationship("Specialization")
  _visits = relationship("Visit", cascade="delete, delete-orphan")
  _timeTable = relationship("TimeElement")

  def __init__(self, nameValue, specializationsValue):
    if not (type(nameValue) is str):
      raise TypeError("Invalid init class Doctor: operand type of nameValue is not str")

    if not (type(specializationsValue) is list) and None:
      raise TypeError("Invalid init class Doctor: operand type of specializationsValue is not list")

    if specializationsValue is not None:
        for i in specializationsValue:
          if not (type(i) is Specialization):
            raise TypeError("Invalid init class Doctor: operand type of one of the elements specializationsValue is not Specialization")

        self._specializations = list(specializationsValue)


    self._fullName = nameValue
    self._visits = []
    self._timeTable = []

  def setFullName(self, value):
    if not (type(value) is str):
      raise TypeError("Invalid call setFullName in class Doctor: operand type of value is not str")

    self._fullName = value

  def getFullName(self):
    return self._fullName

  def setSpecializations(self, value):
    if not (type(value) is list):
      raise TypeError("Invalid call setSpecializations in class Doctor: operand type of value is not list")

    for i in value:
      if not (type(i) is Specialization):
        raise TypeError("Invalid call setSpecializations in class Doctor: operand type of one of the elements value is not Specialization")

    self._specializations = list(value)

  def getSpecializations(self):
    return list(self._specializations)

  def setTimeElements(self, value):
    if not (type(value) is list):
      raise TypeError("Invalid call setTimeElements in class Doctor: operand type of value is not list")

    for i in value:
      if not (type(i) is TimeElement):
        raise TypeError("Invalid call setTimeElements in class Doctor: operand type of one of the elements value is not TimeElement")

    self._timeTable = list(value)

  def getTimeElements(self):
    return list(self._timeTable)

  def getVisits(self):
    return list(self._visits)

  def getFreeTime(self, usedDate):
    if not (type(usedDate) is date):
      raise TypeError("Invalid call getFreeTime in class Doctor: operand type of usedDate is not date")

    freeElements = []
    dateVisits = list(self._visits)
    dateTimeElements = list(self._timeTable)

    #remove all visits with another date
    for i in self._visits:
      if (usedDate != i.getDate()):
        dateVisits.remove(i)

    #remove all TimeElement with another weekday
    for i in self._timeTable:
      if (usedDate.weekday() != i.getWeekDay()):
        dateTimeElements.remove(i)

    #add in list freeElements coreteges with start time end service time that free for visits
    for i in dateTimeElements:
      for j in range(i.getVisitsCount()):
        startTime = datetime(2000, 10, 1, i.getStartTime().hour,
            i.getStartTime().minute, i.getStartTime().second)
        currentTime = timedelta(minutes = j*i.getSpecialization().getServiceTime())+startTime
        p = False
        for k in dateVisits:
          if (k.getStartTime() == currentTime):
            p = True
        if (not p):
          freeElements.append( (k.id, usedDate, currentTime, i.getSpecialization().getServiceTime(), self._fullName) )

    return freeElements

  def getPermittedTime(self, usedDate, spec, patient):
    if not (type(usedDate) is date):
      raise TypeError("Invalid call getPermittedTime in class Doctor: operand type of usedDate is not date")

    if not (type(patient) is Patient):
      raise TypeError("Invalid call getPermittedTime in class Doctor: operand type of patient is not Patient")

    if not (type(spec) is Specialization):
      raise TypeError("Invalid call getPermittedTime in class Doctor: operand type of spec is not Specialization")

    permittedElements = []
    dateVisits = list(self._visits)
    dateTimeElements = list(self._timeTable)
    enrolledSpecs = []

    #remove all visits with another date
    for i in self._visits:
      if (usedDate != i.getDate()):
        dateVisits.remove(i)

    #remove all TimeElement with another weekday
    print(usedDate.weekday())
    for i in self._timeTable:
      print(i.getWeekDay())

      if (usedDate.weekday() != i.getWeekDay()):
        dateTimeElements.remove(i)

    #remove all TimeElement with another specialization
    for i in dateTimeElements:
      if (spec != i.getSpecialization()):
        dateTimeElements.remove(i)

    #add specializations that patients already enroll in this day
    for i in dateVisits:
      if (patient == i.getPatient()):
        enrolledSpecs.append(i.getSpecialization())

    #remove all elements with specialisation that already enrolled in this day by this patient
    for i in dateTimeElements:
      for j in enrolledSpecs:
        if i.getSpecialization() == j:
          dateTimeElements.remove(i)

    #add in list freeElements coreteges with start time end service time that free for visits
    for i in dateTimeElements:
      for j in range(i.getVisitsCount()):
        startTime = datetime(2000, 10, 1, i.getStartTime().hour,
                    i.getStartTime().minute, i.getStartTime().second)
        currentDTime = timedelta(minutes = j*i.getSpecialization().getServiceTime())+startTime
        p = False
        for k in dateVisits:
          if (datetime(2000, 10, 1, k.getStartTime().hour,k.getStartTime().minute,k.getStartTime().second) == currentDTime):
            p = True
        if (not p):
          currentTime = time(currentDTime.hour, currentDTime.minute, currentDTime.second)
          permittedElements.append( (usedDate, currentTime, i.getSpecialization().getServiceTime(), self._fullName) )

    return permittedElements


  def getEngagedTime(self, usedDate, spec, patient):
    if not (type(usedDate) is date):
      raise TypeError("Invalid call getEngagedTime in class Doctor: operand type of usedDate is not date")

    if not (type(patient) is Patient):
      raise TypeError("Invalid call getEngagedTime in class Doctor: operand type of patient is not Patient")

    if not (type(spec) is Specialization):
      raise TypeError("Invalid call getEngagedTime in class Doctor: operand type of spec is not Specialization")

    dateVisits = list(self._visits)

    for i in dateVisits:
      if (patient != i.getPatient()):
        dateVisits.remove(i)

    for i in dateVisits:
      if (usedDate != i.getDate()):
        dateVisits.remove(i)

    for i in dateVisits:
      if (spec != i.getSpecialization()):
        dateVisits.remove(i)

    engagedTime = []
    for i in dateVisits:
      engagedTime.append( (usedDate, i.getStartTime(), i.getSpecialization().getServiceTime()) )

    return engagedTime


  def enrollPatient(self, patient, usedDate, startTime):
    if not (type(patient) is Patient):
      raise TypeError("Invalid call enrollPatient in class Doctor: operand type of patient is not Patient")

    if not (type(usedDate) is date):
      raise TypeError("Invalid call enrollPatient in class Doctor: operand type of usedDate is not date")

    if not (type(startTime) is time):
      raise TypeError("Invalid call enrollPatient in class Doctor: operand type of startTime is not time")

    #find specialization int timeTalbe, if it's don't exist then don't exist this time in time table
    weekDay = usedDate.weekday()

    for i in self._timeTable:
      if (i.getWeekDay() == weekDay) and (startTime >= i.getStartTime()) and (startTime < i.getEndTime()):
        currentSpec = i.getSpecialization()
        break

    if not 'currentSpec' in locals():
      raise RuntimeError("Invalid call enrollPatient in class Doctor: don't exist this time in time table")

    #remove all visits with another date
    dateVisits = self._visits.copy()
    for i in self._visits:
      if (usedDate != i.getDate()):
        dateVisits.remove(i)


    #check that this time not enrolled
    p = False
    for i in dateVisits:
      if (i.getStartTime() == startTime):
        p = True

    if p:
      raise RuntimeError("Invalid call enrollPatient in class Doctor: this time already enrolled")

    #check that patient isn't enrolled in this specialization in this date
    p = False
    for i in dateVisits:
      if (i.getPatient() == patient) and (currentSpec == i.getSpecialization()):
        p = True

    if p:
      raise RuntimeError("Invalid call enrollPatient in class Doctor: this patient already enrolled in this date with this specialization")

    currentVisit = Visit(startTime, usedDate, currentSpec, patient)
    self._visits.append(currentVisit)


  def unenrollPatient(self, patient, usedDate, startTime):
    if not (type(patient) is Patient):
      raise TypeError("Invalid call unenrollPatient in class Doctor: operand type of patient is not Patient")

    if not (type(usedDate) is date):
      raise TypeError("Invalid call unenrollPatient in class Doctor: operand type of usedDate is not date")

    if not (type(startTime) is time):
      raise TypeError("Invalid call unenrollPatient in class Doctor: operand type of startTime is not time")

    #check that patient is rolled in this time in this date and remove it
    p = False
    for i in self._visits:
      if (usedDate == i.getDate()) and (patient == i.getPatient()) and (startTime == i.getStartTime()):
        self._visits.remove(i)
        p = True
        break

    if not p:
      raise RuntimeError("Invalid call unenrollPatient in class Doctor: this patient don't enrolled in this date in this time")

