#Khrapov A. S. M17-512
#Encoding UTF-8

from datetime import date, time, datetime

from timeelement import TimeElement
from patient import Patient
from specialization import Specialization
from doctor import Doctor
from database import *
from visit import Visit


class DBGateway:
  def __init__(self):
    pass

###SAVE
  def savePatient(self, patient):
    if not (type(patient) is Patient):
        raise TypeError("Invalid call savePatient: operand type of patient is not Patient")  
    db_session.add(patient)
    db_session.commit()



  def saveSpecialization(self, specialization):
    if not (type(specialization) is Specialization):
        raise TypeError("Invalid call saveSpecialization: operand type of specialization is not Specialization")

    db_session.add(specialization)
    db_session.commit()

  def saveDoctor(self, doctor):
    if not (type(doctor) is Doctor):
        raise TypeError("Invalid call saveDoctor: operand type of doctor is not Doctor")
    for visit in doctor._visits:
      db_session.add(visit)
    db_session.add(doctor)
    db_session.commit()


  def saveTime(self, timeElement):
    if not (type(timeElement) is TimeElement):
      raise TypeError("Invalid call saveTime: operand type of doctor is not TimeElement")

    db_session.add(timeElement)
    db_session.commit()


  ###EXIST
  def existSpecialization(self, specName):
    if not (type(specName) is str):
      raise TypeError("Invalid call existSpecialization: operand type of specName is not str")

    first = Specialization.query.filter_by(_name=specName).first()
    return first is not None

  def existPatient(self, patientName):
    if not (type(patientName) is str):
      raise TypeError("Invalid call existPatient: operand type of patientName is not str")  

    first = Patient.query.filter_by(_fullName=patientName).first()
    return first is not None

  def existVisitPatient(self, patientName, usedDate, usedTime):
    if not (type(patientName) is str):
      raise TypeError("Invalid call existVisitPatient: operand type of patientName is not str")
    if not (type(usedDate) is (date or datetime)):
      raise TypeError("Invalid call existVisitPatient: operand type of usedDate is not date")
    if not (type(usedTime) is (time or datetime)):
      raise TypeError("Invalid call existVisitPatient: operand type of usedTime is not time")

    visits = Visit.query.filter_by(_date=usedDate).all()

    arr = list(filter(lambda v: v.getPatient().getFullName() == patientName and
                                                         v.getStartTime() == usedTime, visits))
    return len(arr) != 0

  def existDoctor(self, doctorName):
    if not (type(doctorName) is str):
      raise TypeError("Invalid call savePatient: operand type of doctorName is not str")

    first = Doctor.query.filter_by(_fullName=doctorName).first()
    return first is not None

###LOAD
  def loadAllSpecializationsName(self):
    return list(map(lambda s : s.getName(), Specialization.query.all()))

  def loadSpecialization(self, specName):
    if not (type(specName) is str):
      raise TypeError("Invalid call loadSpecialization: operand type of specName is not str")

    return Specialization.query.filter_by(_name=specName).first()

  def loadPatient(self, patientName):
    if not (type(patientName) is str):
      raise TypeError("Invalid call loadPatient: operand type of patientName is not str")

    return Patient.query.filter_by(_fullName=patientName).first()

  def loadDoctor(self, doctorName):
    if not (type(doctorName) is str):
      raise TypeError("Invalid call loadDoctor: operand type of doctorName is not str")

    return Doctor.query.filter_by(_fullName=doctorName).first()

  def loadDoctorsNameBySpecialization(self, specName):
    if not (type(specName) is str):
      raise TypeError("Invalid call loadDoctorsNameBySpecialization: operand type of specName is not str")

    return list(map(lambda x: x.getFullName(), Doctor.query.filter(Specialization._name.endswith(specName)).all()))

  def loadVisitByPatientName(self, patientName):
    if not (type(patientName) is str):
      raise TypeError("Invalid call loadVisitByPatientName: operand type of specName is not str")
    visit = Visit.query.filter(Patient._fullName == patientName).all()
    res = list(filter(lambda x: x._patient.getFullName() == patientName, visit))
    return res

  def loadDoctorById(self, doctor_id):
    return Doctor.query.filter(Doctor.id.like(doctor_id)).first()
