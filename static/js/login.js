/**
 * Created by aleksejpluhin on 10.11.16.
 */


$(document).on("click", "#send-login", function () {
    var username = $("#login-login").val();
    var password = $("#password-login").val();

     $.ajax({
            type: "POST",
            url: '/patient/login',
            dataType: 'json',
            contentType : "application/json;charset=utf-8",
            data: JSON.stringify(
                {
                    name: username,
                }),
            complete: function (d) {
                setTimeout(window.location.replace("/index"), 150); //not user
            }
        })
})
