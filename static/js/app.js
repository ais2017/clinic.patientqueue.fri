/**
 * Created by aleksejpluhin on 19.12.17.
 */


var ListSpecialization = React.createClass({

    getInitialState: function () {

        return {
            index: 0,
            specializations: []
        };
    },

    componentDidMount: function () {
        $.get('/specialization/all').done(function (data) {
            this.setState({specializations: data});
        }.bind(this));

    },


    renderTimeTable: function (e) {
        ReactDOM.render(
            <TimeTable specName={e.target.text}/>,
            document.getElementById('test2')
        )
    },

    render: function () {
        return (
            <div className="container col-md-4">
                <ul className="list-group">
                    <p>Специализации</p>
                    {
                        this.state.specializations.map(function (current, index) {
                            return <li className="list-group-item" key={index}><a value={current}
                                                                                  onClick={(index) => this.renderTimeTable(index)}>{current}</a>
                            </li>
                        }, this)
                    }
                </ul>
            </div>)


    },


})

var TimeTable = React.createClass({

    getInitialState: function () {
        return {
            timeTable: [],
            currentType: this.props.specName,
            patientName: null
        };
    },

    componentDidMount: function () {
        var str = '/doctor/free?specName=' + this.state.specName + '&patientName=Patient1'
        $.get(str).done(function (data) {
            var date = (JSON).parse(data)
            this.setState({timeTable: date});
        }.bind(this));

        $.get("/patient/login").done(function (data) {
            this.setState({patientName: data});
        }.bind(this));

    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps == undefined) return
        console.log(nextProps)
        var str = '/doctor/free?specName=' + nextProps.specName + '&patientName=Patient1'
        $.get(str).done(function (data) {
            var res = (JSON).parse(data)
            var result = []
            $.each(res, function (index, item) {
                var obj = {}
                var date = new Date((item[1] - 10800) * 1000)
                obj.date = date
                obj.name = item[0]
                result.push(obj)
            })
            this.setState({
                timeTable: result,
                currentType: nextProps.specName
            });
        }.bind(this));
    },


    render: function () {
        if (this.state.timeTable.length == 0) {

            return <div className="container col-md-8">
                <ul className="list-group">
                    <p>Свободные места</p>
                    <li className="list-group-item"><a>Свободных мест нет</a></li>
                </ul>
            </div>
        } else {
            return (<div className="container col-md-8">
                <ul className="list-group">
                    <p>Свободные места</p>

                    {
                        this.state.timeTable.map(function (current, index) {
                            return <li className="list-group-item" key={index} value={current}
                                       onClick={(event) => this.sendTime(current, event)}>
                                Доктор: {current.name} <br/>
                                Дата: <b>{current.date.toDateString()}</b> Время:
                                <b>{current.date.toLocaleTimeString()}</b>
                            </li>
                        }, this)
                    }
                </ul>
            </div>)
        }
    },


    sendTime: function (current, event) {
        var type = this.state.currentType
        console.log(type)
        var state = this
        var timestamp = current.date.getTime() / 1000;
        $.ajax({
            type: "POST",
            url: '/specialization/select',
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            data: JSON.stringify(
                {
                    type: this.state.currentType,
                    doctorName: current.name,
                    dateTime: timestamp
                }),
            complete: function (data) {
                var str = '/doctor/free?specName=' + type + '&patientName=Patient1'
                $.get(str).done(function (data) {
                    var res = (JSON).parse(data)
                    var result = []
                    $.each(res, function (index, item) {
                        var obj = {}
                        var date = new Date((item[1] - 10800) * 1000)
                        obj.date = date
                        obj.name = item[0]
                        result.push(obj)
                    })
                    state.setState({timeTable: result});
                }.bind(state));
            }
        })
    }

})

var Menu = React.createClass({

    toPatientCabinet: function () {
      window.location.replace("/patient")
    },

    render: function () {
        return <div className="container">
            <div><a onClick={this.toPatientCabinet}>Зарезервированое время</a></div>
        </div>
    }

})

ReactDOM.render(
    <ListSpecialization/>,
    document.getElementById('root')
)

ReactDOM.render(
    <Menu/>,
    document.getElementById('head')
)

