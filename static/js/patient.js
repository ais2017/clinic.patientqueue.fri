/**
 * Created by aleksejpluhin on 21.12.17.
 */



var Registrations = React.createClass({

    getInitialState: function () {
        return {
            currentRegistration: [],
            patientName: null
        }
    },

    componentDidMount: function () {
        $.get('/patient/registrations?patientName=' + this.state.patientName).done(function (data) {
            this.setState({currentRegistration: data});
        }.bind(this));

          $.get("/patient/login").done(function (data) {
            this.setState({patientName: data});
        }.bind(this));


    },

    sendUneroll: function (current, event) {
        var state = this
        $.ajax({
            type: "POST",
            url: '/doctor/uneroll',
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            data: JSON.stringify(
                {
                    type: current.specialization,
                    doctorName: current.doctorName,
                    dateTime: current.currentDate - 10800,
                    patientName: this.state.patientName
                }),
            complete: function (data) {
                console.log(state)
                $.get('/patient/registrations?patientName=' + state.state.patientName).done(function (data) {
                    this.setState({currentRegistration: data});
                }.bind(state));
            }
        })
    },

    render: function () {
        return (
            <div className="container">
                <ul className="list-group">
                    <p>Текущие записи пациента {this.state.patientName}</p>
                    {
                        this.state.currentRegistration.map(function (item, index) {
                            var currentDate = new Date((item.currentDate - 10800) * 1000)
                            return <li className="list-group-item" onClick={(event) => this.sendUneroll(item, event)}>
                                <p>Дата {currentDate.toDateString()}</p>
                                <p>Время {currentDate.toTimeString()}</p>
                                <p>Врач {item.doctorName} Специализация {item.specialization}</p>
                            </li>
                        }, this)
                    }
                </ul>
            </div>
        )
    }

})

var Menu = React.createClass({

    toPatientCabinet: function () {
      window.location.replace("/index")
    },

    render: function () {
        return <div className="container">
            <div><a onClick={this.toPatientCabinet}>К выбору времени</a></div>
        </div>
    }

})


ReactDOM.render(
    <Registrations/>,
    document.getElementById('registrations')
)


ReactDOM.render(
    <Menu/>,
    document.getElementById('head')
)