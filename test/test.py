#Khrapov A. S. M17-512
#Encoding UTF-8

import sys
import unittest

from datetime import date, time, timedelta, datetime

sys.path.insert(0, '../src')
from patient import Patient
from specialization import Specialization
from visit import Visit
from timeelement import TimeElement
from doctor import Doctor

class TestPatientClass(unittest.TestCase):

  def test_initPatient(self):
    a = Patient("Кулаков Алексей Дмитриевич", "123-456")
    self.assertEqual(a.getFullName(), "Кулаков Алексей Дмитриевич")
    self.assertEqual(a.getOms(), "123-456")

    with self.assertRaises(TypeError):
      b = Patient(123, "str")

    with self.assertRaises(TypeError):
      b = Patient("String", (2,5))

    with self.assertRaises(TypeError):
      b = Patient([], 1)

  def test_setFullName(self):
    a = Patient("Поляков Сергей Генадьевич", "888-222")
    a.setFullName("Кулаков Алексей Дмитриевич")

    self.assertEqual(a.getFullName(), "Кулаков Алексей Дмитриевич")

    with self.assertRaises(TypeError):
      a.setFullName(["Поляков", "Сергей", "Юрьевич"])

  def test_setOms(self):
    a = Patient("Поляков Сергей Генадьевич", "888-222")
    a.setOms("145-325")

    self.assertEqual(a.getOms(), "145-325")

    with self.assertRaises(TypeError):
      a.setOms(135485)


class TestSpecializationClass(unittest.TestCase):
  def test_initSpecialization(self):
    a = Specialization("Хирург", 10)
    self.assertEqual(a.getName(), "Хирург")
    self.assertEqual(a.getServiceTime(), 10)

    with self.assertRaises(TypeError):
      b = Specialization(123, "str")

    with self.assertRaises(TypeError):
      b = Specialization("String", "str")

    with self.assertRaises(TypeError):
      b = Specialization(145, 189)

    with self.assertRaises(ValueError):
      b = Specialization("Терапевт", 0)

    with self.assertRaises(ValueError):
      b = Specialization("Терапевт", -5)

    with self.assertRaises(ValueError):
      b = Specialization("Терапевт", 1440)

  def test_setName(self):
    a = Specialization("Хирург", 10)
    a.setName("Травмотолог")

    self.assertEqual(a.getName(), "Травмотолог")

    with self.assertRaises(TypeError):
      a.setName(["Уролог", "Окулист"])

  def test_setServiceTime(self):
    a = Specialization("Хирург", 10)
    a.setServiceTime(20)

    self.assertEqual(a.getServiceTime(), 20)

    with self.assertRaises(TypeError):
      a.setServiceTime("15")
  
    with self.assertRaises(ValueError):
      a.setServiceTime(0)

    with self.assertRaises(ValueError):
      a.setServiceTime(-2)

    with self.assertRaises(ValueError):
      a.setServiceTime(1441)


class TestVisitClass(unittest.TestCase):
  def test_initVisit(self):
    patient = Patient("Юрков Алексей Владимирович", "123-455")
    spec = Specialization("Терапевт", 10)
    a = Visit(time(8, 30), date(2017,10,2), spec, patient)

    self.assertEqual(a.getDate(), date(2017,10,2))
    self.assertEqual(a.getStartTime(), time(8, 30))
    self.assertEqual(a.getSpecialization(), spec)
    self.assertEqual(a.getPatient(), patient)
    self.assertEqual(a.getWeekDay(), date(2017,10,2).weekday())

    with self.assertRaises(TypeError):
      b = Visit(datetime(2000, 10, 1, hour=8, minute=30), date(2017,10,2), spec, patient)

    with self.assertRaises(TypeError):
      b = Visit(time(8, 30), "2017:10:2", spec, patient)

    with self.assertRaises(TypeError):
      b = Visit(time(8, 30), date(2017,10,2), "Стрихолог", patient)

    with self.assertRaises(TypeError):
      b = Visit(time(8, 30), date(2017,10,2), spec, "Иванов Дмитрий Сергеевич")

  def test_setDate(self):
    patient = Patient("Юрков Алексей Владимирович", "123-455")
    spec = Specialization("Терапевт", 10)
    a = Visit(time(8, 30), date(2017,10,2), spec, patient)

    a.setDate(date(2017,1,1))
  
    self.assertEqual(a.getDate(), date(2017,1,1))
    self.assertEqual(a.getWeekDay(), date(2017,1,1).weekday())

    with self.assertRaises(TypeError):
      a.setDate(("2017", "3", "3"))

  def test_setSpecialization(self):
    patient = Patient("Юрков Алексей Владимирович", "123-455")
    spec = Specialization("Терапевт", 10)
    a = Visit(time(8, 30), date(2017,10,2), spec, patient)

    newSpec = Specialization("Офтольмолог", 20)
    a.setSpecialization(newSpec)
  
    self.assertEqual(a.getSpecialization(), newSpec)

    with self.assertRaises(TypeError):
      a.setSpecialization("Офтольмолог")

  def test_setStartTime(self):
    patient = Patient("Юрков Алексей Владимирович", "123-455")
    spec = Specialization("Терапевт", 10)
    a = Visit(time(8, 30), date(2017,10,2), spec, patient)

    a.setStartTime(time(9, 20))
  
    self.assertEqual(a.getStartTime(), time(9, 20))

    with self.assertRaises(TypeError):
      a.setStartTime("8:50")

  def test_setPatient(self):
    patient = Patient("Юрков Алексей Владимирович", "123-455")
    spec = Specialization("Терапевт", 10)
    a = Visit(time(8, 30), date(2017,10,2), spec, patient)

    newPatient = Patient("Рондагин Дмитрий Фёдорович", "421-234")
    a.setPatient(newPatient)
  
    self.assertEqual(a.getPatient(), newPatient)

    with self.assertRaises(TypeError):
      a.setPatient(["Рокин", "132-312"])

class TestTimeElementClass(unittest.TestCase):
  def test_initVisit(self):
    spec = Specialization("Терапевт", 15)
    a = TimeElement(date(2017,12,5).weekday(), time(8, 30), 5, spec)

    #endTime
    endDateTime = datetime(1989, 1, 1, hour=8,minute=30)
    endDateTime += 5*timedelta(minutes=15)
    endTime = time(hour=endDateTime.hour, minute=endDateTime.minute)
      

    #date(2017,12,5).weekday() = 1 Thu
    self.assertEqual(a.getWeekDay(), 1)
    self.assertEqual(a.getStartTime(), time(8, 30))
    self.assertEqual(a.getVisitsCount(), 5)
    self.assertEqual(a.getEndTime(), endTime)
    self.assertEqual(a.getSpecialization(), spec)

    with self.assertRaises(TypeError):
      b = TimeElement("Mon", time(8, 30), 5, spec)

    with self.assertRaises(TypeError):
      b = TimeElement(date(2017,12,5).weekday(), "8:30", 5, spec)

    with self.assertRaises(TypeError):
      b = TimeElement(date(2017,12,5).weekday(), time(8, 30), "5", spec)

    with self.assertRaises(TypeError):
      b = TimeElement(date(2017,12,5).weekday(), time(8, 30), 5, "Уролог")

    with self.assertRaises(ValueError):
      b = TimeElement(7, time(8, 30), 5, spec)

    with self.assertRaises(ValueError):
      b = TimeElement(-1, time(8, 30), 5, spec)

    with self.assertRaises(ValueError):
      b = TimeElement(date(2017,12,5).weekday(), time(8, 30), -1, spec)

  def test_setWeekDay(self):
    spec = Specialization("Терапевт", 15)
    a = TimeElement(date(2017,12,5).weekday(), time(8, 30), 5, spec)
    a.setWeekDay(2)

    self.assertEqual(a.getWeekDay(), 2)

    with self.assertRaises(TypeError):
      a.setWeekDay("2")

    with self.assertRaises(ValueError):
      a.setWeekDay(-1)

    with self.assertRaises(ValueError):
      a.setWeekDay(7)

  def test_setStartTime(self):
    spec = Specialization("Терапевт", 15)
    a = TimeElement(date(2017,12,5).weekday(), time(8, 30), 5, spec)
    a.setStartTime(time(9, 0))

    #endTime
    endDateTime = datetime(1989, 1, 1, hour=9,minute=0)
    endDateTime += 5*timedelta(minutes=15)
    endTime = time(hour=endDateTime.hour, minute=endDateTime.minute)

    self.assertEqual(a.getStartTime(), time(9, 0))
    self.assertEqual(a.getEndTime(), endTime)

    with self.assertRaises(TypeError):
      a.setStartTime("9:00")

  def test_setVisitsCount(self):
    spec = Specialization("Терапевт", 15)
    a = TimeElement(date(2017,12,5).weekday(), time(8, 30), 5, spec)
    a.setVisitsCount(3)

    #endTime
    endDateTime = datetime(1989, 1, 1, hour=8,minute=30)
    endDateTime += 3*timedelta(minutes=15)
    endTime = time(hour=endDateTime.hour, minute=endDateTime.minute)

    self.assertEqual(a.getVisitsCount(), 3)
    self.assertEqual(a.getEndTime(), endTime)

    with self.assertRaises(TypeError):
      a.setVisitsCount("30")

    with self.assertRaises(ValueError):
      a.setVisitsCount(0)

    with self.assertRaises(ValueError):
      a.setVisitsCount(-5)

  def test_setSpecialization(self):
    patient = Patient("Юрков Алексей Владимирович", "123-455")
    spec = Specialization("Терапевт", 10)
    a = TimeElement(date(2017,12,5).weekday(), time(8, 30), 5, spec)

    newSpec = Specialization("Офтольмолог", 20)
    a.setSpecialization(newSpec)
  
    self.assertEqual(a.getSpecialization(), newSpec)

    with self.assertRaises(TypeError):
      a.setSpecialization("Офтольмолог")

class TestDoctorClass(unittest.TestCase):
  def test_initDoctor(self):
    spec1 = Specialization("Венеролог", 15)
    spec2 = Specialization("Стрихолог", 15)
    specs = [spec1, spec2]
    a = Doctor("Боляев Алексей Дмитриевич", specs)

    self.assertEqual(a.getFullName(), "Боляев Алексей Дмитриевич")
#    for i in range(size(specs):
#      self.assertEqual(a.getSpecializations(), "Боляев Алексей Дмитриевич")

    with self.assertRaises(TypeError):
      b = Doctor(["Негуляев Дмитрий Владимирович"], specs)

    with self.assertRaises(TypeError):
      b = Doctor("Негуляев Дмитрий Владимирович", spec1)

    with self.assertRaises(TypeError):
      b = Doctor("Негуляев Дмитрий Владимирович", [spec1, "Терапевт"])

  def test_setFullName(self):
    spec1 = Specialization("Венеролог", 15)
    spec2 = Specialization("Стрихолог", 15)
    specs = [spec1, spec2]
    a = Doctor("Боляев Алексей Дмитриевич", specs)

    a.setFullName("Айболитов Юрий Владимирович")

    self.assertEqual(a.getFullName(), "Айболитов Юрий Владимирович")

    with self.assertRaises(TypeError):
      a.setFullName(["Поляков", "Сергей", "Юрьевич"])

  def test_setSpecializations(self):
    spec1 = Specialization("Венеролог", 15)
    spec2 = Specialization("Стрихолог", 15)
    specs = [spec1, spec2]
    a = Doctor("Боляев Алексей Дмитриевич", specs)

    specs.append(Specialization("Дерматолог", 10))
    a.setSpecializations(specs)

    self.assertEqual(len(a.getSpecializations()), len(specs))

    for i in range(len(specs)):
      self.assertEqual(a.getSpecializations()[i], specs[i])

    with self.assertRaises(TypeError):
      a.setSpecializations(spec1)

    with self.assertRaises(TypeError):
      a.setSpecializations([spec1, "Дерматолог"])

  def test_setTimeElements(self):
    spec1 = Specialization("Венеролог", 15)
    spec2 = Specialization("Стрихолог", 15)
    specs = [spec1, spec2]
    a = Doctor("Боляев Алексей Дмитриевич", specs)

    times = [TimeElement(0, time(8,30), 30, spec1), TimeElement(2, time(12,30), 10, spec2)]
    
    a.setTimeElements(times)

    self.assertEqual(len(a.getTimeElements()), len(times))

    for i in range(len(times)):
      self.assertEqual(a.getTimeElements()[i], times[i])

    with self.assertRaises(TypeError):
      a.setTimeElements(TimeElement(0, time(8,30), 30, spec1))

    with self.assertRaises(TypeError):
      a.setTimeElements([TimeElement(0, time(8,30), 30, spec1), "8:30"])


  def test_getPermittedTime(self):
    spec1 = Specialization("Венеролог", 15)
    spec2 = Specialization("Стрихолог", 15)
    specs = [spec1, spec2]
    a = Doctor("Боляев Алексей Дмитриевич", specs)

    times = [TimeElement(0, time(8,30), 30, spec1), TimeElement(2, time(12,30), 4, spec2), 
              TimeElement(2, time(16,0), 2, spec1)]
    a.setTimeElements(times)

    patient = Patient("Юркин Дмитрий Алексеевич", "242-124")
    permit = a.getPermittedTime(date(2017,12,6), spec2, patient)

    self.assertEqual(len(permit), 4)
    
    d = date(2017,12,6)
    testPermit = [(d, time(12,30), 15), (d, time(12,45), 15), (d, time(13, 0), 15), (d, time(13, 15), 15)]
    for i in range(len(testPermit)):
      self.assertEqual(permit[i], testPermit[i])

    with self.assertRaises(TypeError):
      a.setTimeElements(TimeElement(0, time(8,30), 30, spec1))

    with self.assertRaises(TypeError):
      a.setTimeElements([TimeElement(0, time(8,30), 30, spec1), "8:30"])

  def test_enrollPatient(self):
    spec1 = Specialization("Венеролог", 15)
    spec2 = Specialization("Стрихолог", 15)
    specs = [spec1, spec2]
    a = Doctor("Боляев Алексей Дмитриевич", specs)

    times = [TimeElement(0, time(8,30), 30, spec1), TimeElement(2, time(12,30), 4, spec2), 
              TimeElement(2, time(16,0), 2, spec1)]
    a.setTimeElements(times)

    patient = Patient("Юркин Дмитрий Алексеевич", "242-124")
    a.enrollPatient(patient, date(2017,12,6), time(12,45))

    permit = a.getPermittedTime(date(2017,12,6), spec2, patient)  
    self.assertEqual(len(permit), 0)
    
    permit = a.getPermittedTime(date(2017,12,6), spec1, patient)
    self.assertEqual(len(permit), 2)
    d = date(2017,12,6)
    testPermit = [(d, time(16, 0), 15), (d, time(16, 15), 15)]
    for i in range(len(testPermit)):
      self.assertEqual(permit[i], testPermit[i])

    with self.assertRaises(TypeError):
      a.enrollPatient("Юркин Дмитрий Алексеевич", date(2017,12,6), time(12,45))

    with self.assertRaises(TypeError):
      a.enrollPatient(patient, "2017,12,6", time(12,45))

    with self.assertRaises(TypeError):
      a.enrollPatient(patient, date(2017,12,6), "12,45")


  def test_getEngagedTime(self):
    spec1 = Specialization("Венеролог", 15)
    spec2 = Specialization("Стрихолог", 15)
    specs = [spec1, spec2]
    a = Doctor("Боляев Алексей Дмитриевич", specs)

    times = [TimeElement(0, time(8,30), 30, spec1), TimeElement(2, time(12,30), 4, spec2), 
              TimeElement(2, time(16,0), 2, spec1)]
    a.setTimeElements(times)

    patient = Patient("Юркин Дмитрий Алексеевич", "242-124")
    a.enrollPatient(patient, date(2017,12,6), time(12,45))
    
    engaged = a.getEngagedTime(date(2017,12,6), spec2, patient)

    self.assertEqual(len(engaged), 1)
    d = date(2017,12,6)
    testEngaged = [(d, time(12, 45), 15)]
    for i in range(len(testEngaged)):
      self.assertEqual(engaged[i], testEngaged[i])

    with self.assertRaises(TypeError):
      a.getEngagedTime("2017,12,6", spec2, patient)

    with self.assertRaises(TypeError):
      a.getEngagedTime(date(2017,12,6), "Венеролог", patient)

    with self.assertRaises(TypeError):
      a.getEngagedTime(date(2017,12,6), spec2, "Юркин Дмитрий Алексеевич")


  def test_unenrollPatient(self):
    spec1 = Specialization("Венеролог", 15)
    spec2 = Specialization("Стрихолог", 15)
    specs = [spec1, spec2]
    a = Doctor("Боляев Алексей Дмитриевич", specs)

    times = [TimeElement(0, time(8,30), 30, spec1), TimeElement(2, time(12,30), 4, spec2), 
              TimeElement(2, time(16,0), 2, spec1)]
    a.setTimeElements(times)

    patient = Patient("Юркин Дмитрий Алексеевич", "242-124")
    a.enrollPatient(patient, date(2017,12,6), time(12,45))
    
    engaged = a.getEngagedTime(date(2017,12,6), spec2, patient)

    self.assertEqual(len(engaged), 1)
    d = date(2017,12,6)
    testEngaged = [(d, time(12, 45), 15)]
    for i in range(len(testEngaged)):
      self.assertEqual(engaged[i], testEngaged[i])


    a.unenrollPatient(patient, date(2017,12,6), time(12,45))

    engaged = a.getEngagedTime(date(2017,12,6), spec2, patient)
    self.assertEqual(len(engaged), 0)

    with self.assertRaises(TypeError):
      a.unenrollPatient("Юркин Дмитрий Алексеевич", date(2017,12,6), time(12,45))

    with self.assertRaises(TypeError):
      a.unenrollPatient(patient, "2017,12,6", time(12,45))

    with self.assertRaises(TypeError):
      a.unenrollPatient(patient, date(2017,12,6), "12,45")


if __name__ == '__main__':
    unittest.main() 
